import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  message = 'My Example';

  constructor(private modalCtrl: ModalController) {}

  async openModal(){
    const modal = await this.modalCtrl.create({
      component: ModalComponent,
      backdropDismiss: false,
      id: '1234'
    });
    modal.present();

    const modalResult = await modal.onWillDismiss();
    const data = modalResult.data;
    const role = modalResult.role;

    // const {data, role} = await modal.onWillDismiss(); => Object destructuring

    if(role == 'confirm'){
      if(data){
        this.message = 'Hello, ' + data + '.';
      }else{
        this.message = 'Hello!'
      }
    }
  }
}
