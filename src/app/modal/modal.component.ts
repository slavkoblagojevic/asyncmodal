import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent  implements OnInit {
  name!: string;
  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {}

  cancel(){
    return this.modalCtrl.dismiss(null, 'cancel', '1234');
  }

  confirm(){
    return this.modalCtrl.dismiss(this.name, 'confirm', '1234');
  }
}
